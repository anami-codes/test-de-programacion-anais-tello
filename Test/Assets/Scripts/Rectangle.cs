﻿using UnityEngine;
using System.Collections;

public class Rectangle {


    string id;
    string name;
    Vector2 centralPos;
    float w, h;
    Vector2 min, max;

    public Rectangle () {
        id = "";
        name = "";
        centralPos = Vector2.zero;
        w = 1;
        h = 1;
        GetEnds();
    }

    public Rectangle(string name_, Vector2 centralPos_, float w_, float h_){
        name = name_;
        id = "Rectangle" + name;
        centralPos = centralPos_;
        if (w_ == 0) w_ = 1;
        if (h_ == 0) h_ = 1;
        w = w_;
        h = h_;
        GetEnds();
    }

    public string GetName () {
        return name;
    }

    public Vector2 GetCentralPos () {
        return centralPos;
    }

    public float GetWidth () {
        return w;
    }

    public float GetHeight() {
        return h;
    }

    public Vector2 GetMin () {
        return min;
    }

    public Vector2 GetMax () {
        return max;
    }

    public void GetEnds () {
        float w_ = w / 2;
        float h_ = h / 2;
        max = new Vector2(w_, h_);
        min = new Vector2(-w_, -h_);
    }

    public static bool RectCollision (Rectangle[] rectangles, int rectID) {
        bool coll = false;

        float maxX = rectangles[rectID].max.x;
        float minX = rectangles[rectID].min.x;
        float maxY = rectangles[rectID].max.y;
        float minY = rectangles[rectID].min.y;

        for (int i = 1; i < rectangles.Length; i++) {
            Vector2 min = rectangles[i].min;
            Vector2 max = rectangles[i].max;
            if (((maxX >= max.x) && (max.x >= minX)) || ((maxX >= min.x) && (min.x >= minX))) {
                if (((maxY >= max.y) && (max.y >= minY)) || ((maxY >= min.y) && (min.y >= minY)))
                    coll = true;
                else
                    coll = false;
            } else {
                coll = false;
            }
        }

        return coll;
    }

    public static bool RectCollisionAll (Rectangle[] rectangles) {
        bool coll = false;

        float maxX = rectangles[0].max.x;
        float minX = rectangles[0].min.x;
        float maxY = rectangles[0].max.y;
        float minY = rectangles[0].min.y;

        for (int i = 1; i < rectangles.Length; i++)
        {
            Vector2 min = rectangles[i].min;
            Vector2 max = rectangles[i].max;
            if (((maxX >= max.x) && (max.x >= minX)) || ((maxX >= min.x) && (min.x >= minX))){
                if ((maxX >= max.x) && (max.x >= minX)) minX = max.x;
                if ((maxX >= min.x) && (min.x >= minX)) minX = min.x;
                if (((maxY >= max.y) && (max.y >= minY)) || ((maxY >= min.y) && (min.y >= minY))) { 
                    if ((maxY >= max.y) && (max.y >= minY)) minY = max.y;
                    if ((maxY >= min.y) && (min.y >= minY)) minY = min.y;
                    coll = true;
                } else
                    coll = false;
            }
            else
            {
                coll = false;
            }
        }

        return coll;
    }
}
