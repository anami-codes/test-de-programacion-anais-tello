﻿using UnityEngine;
using System.Collections;

public class ThirdQuestion : MonoBehaviour {

    public float x;
    public float y;
    public int n;
    public float r;

    public GameObject p;
    public GameObject point;

	void Start () {
        MakeSemiCircle(r, x, y, n);
	}

    void MakeSemiCircle (float r_, float x_, float y_, int n_) {
        p.transform.position = new Vector3(x_, y_, 0); //Posicionar centro
        float rot = 180 / n_; //Calcular número de grados entre punto y punto
        for (int i = 0; i <= n; i++) {
            GameObject point_ = Instantiate(point, p.transform) as GameObject; //Crea el punto
            point_.transform.SetParent(p.transform); //Hace al punto hijo del centro
            point_.transform.localPosition = new Vector3(r, 0, 0); //Coloca al punto a la distancia del radio del centro
            point_.transform.SetParent(null); //Separa el punto del centro
            p.transform.eulerAngles = new Vector3(0, 0, p.transform.eulerAngles.z + rot); //Rota el centro
        }
    }
}
