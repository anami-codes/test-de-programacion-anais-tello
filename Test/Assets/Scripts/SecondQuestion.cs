﻿using UnityEngine;
using System.Collections;

public class SecondQuestion : MonoBehaviour {

    Rectangle[] rectangles = new Rectangle[3];

	void Start () {
        rectangles[0] = new Rectangle("A", Vector3.zero, Random.Range(1,50), Random.Range(1,50));
        rectangles[1] = new Rectangle("B", new Vector2(3,3), Random.Range(1, 50), Random.Range(1, 50));
        rectangles[2] = new Rectangle("C", new Vector2(-3, -3), Random.Range(1, 50), Random.Range(1, 50));

        for (int i = 0; i < rectangles.Length; i++) {
            Debug.Log(rectangles[i].GetName() + " -> " + rectangles[i].GetMin() + " @ " + rectangles[i].GetMax());
        }

        Debug.Log ("Respuesta 2.1 => B y C colosionan con A: " + Rectangle.RectCollision(rectangles, 0));
        Debug.Log("Respuesta 2.2 => A, B y C colosionan entre ellos: " + Rectangle.RectCollisionAll(rectangles));
    }
	
	void Update () {
        
	}
}
